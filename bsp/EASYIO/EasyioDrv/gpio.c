#include "gpio.h"

#include <rtthread.h>
#include <stm32f10x.h>




void init_gpio_func(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA,ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB,ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC,ENABLE);


	GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_Out_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;

	GPIO_InitStructure.GPIO_Pin   = (GPIO_Pin_11|GPIO_Pin_12);
	GPIO_Init(GPIOA, &GPIO_InitStructure);


	GPIO_InitStructure.GPIO_Pin   = (GPIO_Pin_2|GPIO_Pin_3);
	GPIO_Init(GPIOC, &GPIO_InitStructure);

	relay_gpio_ctl(100,0);
	relay_gpio_ctl(101,0);


}

void power_sim900(void)
{

	GPIO_ResetBits(GPIOA,GPIO_Pin_12);
	rt_thread_sleep(100*1);
	GPIO_SetBits(GPIOA,GPIO_Pin_12);
}

void reset_sim900(void)
{
	GPIO_SetBits(GPIOA, GPIO_Pin_11);
	rt_thread_delay( RT_TICK_PER_SECOND * 1 ); /* sleep 2 second and switch to other thread */
	GPIO_ResetBits(GPIOA, GPIO_Pin_11);
	rt_thread_delay(RT_TICK_PER_SECOND * 1);
}

void relay_gpio_ctl(int index , char status)
{
	switch(index)
	{
		case 100:
			if (status)
				GPIO_ResetBits(GPIOC, GPIO_Pin_2);
			else
				GPIO_SetBits(GPIOC, GPIO_Pin_2);
			break;
		case 101:
			if (status)
				GPIO_ResetBits(GPIOC, GPIO_Pin_3);
			else
				GPIO_SetBits(GPIOC, GPIO_Pin_3);
			break;
		default:
			break;

	}
}

