#ifndef __flash_h__
#define __flash_h__

#if 0
#include "stm32f10x_flash.h"

#define FLASH_START_ADDR        ((u32)0x0803E800)//256K
//#define FLASH_START_ADDR        ((u32)0x0801F400)//128K

#define FLASH_CONFIG_BUFFERSIZE 2048 //byte

int write_flash(u32 StartAddr,u16 *buf,u16 len);
int read_flash(u32 StartAddr,u16 *buf,u16 len);

#endif

#endif
